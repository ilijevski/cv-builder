import "./App.css";
import Category from "./Components/Category";
import Home from "./Components/Home";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LastPage from "./Components/LastPage";
import WebDevelopment from "./Components/WebDevelopment";
import DataScience from "./Components/DataScience";
import DigitalMarketing from "./Components/DigitalMarketing";
import Design from "./Components/Design";
import { UserContextProvider } from "./useContext";

function App() {
  return (
    <UserContextProvider>
      <Router>
        <div className="App">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/category/" exact component={Category} />
            <Route path="/lastPage" component={LastPage} />
            <Route path="/category/webDevelopment" component={WebDevelopment} />
            <Route path="/category/dataScience" component={DataScience} />
            <Route
              path="/category/digitalMarketing"
              component={DigitalMarketing}
            />
            <Route path="/category/design" component={Design} />
          </Switch>
        </div>
      </Router>
    </UserContextProvider>
  );
}

export default App;
