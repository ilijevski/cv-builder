import React, { createContext, useState } from "react";
import tips from "./data";
import html2pdf from "html2pdf.js";
import addImg from "./Images/CV images/addimg.png";

export const Context = createContext({});
export const Provider = (props) => {
  const [category, setCategory] = useState("");

  const [fileUpload, setFileUpload] = useState(addImg);

  const [basicInfo, setBasicInfo] = useState(
    tips[0].editWebDevelopment.basicInfo
  );

  const [workExperiance, setWorkExperiance] = useState(
    tips[0].editWebDevelopment.workExperiance
  );

  const [education, setEducation] = useState(
    tips[0].editWebDevelopment.education
  );

  const [achievement, setAchievement] = useState(
    tips[0].editWebDevelopment.achievements
  );

  const [skills, setSkills] = useState(tips[0].editWebDevelopment.skills);

  const [languages, setLanguages] = useState(
    tips[0].editWebDevelopment.languages
  );

  const [showTips, setShowTips] = useState(" ");

  const [dataScienceSkills, setDataScienceSkills] = useState(
    tips[0].editWebDevelopment.dataScienceSkills
  );

  const [informalEducations, setInfromalEducation] = useState(
    tips[0].editWebDevelopment.informalEducations
  );

  const [pdf, setPdf] = useState();

  const [isLinkedSeen, setLinkedSeen] = useState(false);
  const [isLaikaSeen, setLaikaSeen] = useState(false);

  const [editCvMode, setEditCvMode] = useState(false);

  const handleTipsDisplay = (name, location) => {
    setShowTips(tips[0][location][name]);
  };

  const handleFileUpload = (e) => {
    e.target.value && setFileUpload(URL.createObjectURL(e.target.files[0]));
  };

  const handleBasicInfo = (value, name) => {
    setBasicInfo({ ...basicInfo, [name]: value });
  };

  const handleEditLocation = (location) => {
    setCategory(location.split("/")[2]);
  };

  const handleWorkExperiance = (e, id, name) => {
    const index = workExperiance.findIndex((el) => {
      return el.id === id;
    });
    const newWorkExp = [...workExperiance];
    newWorkExp[index][name] = e.target.value;
    setWorkExperiance(newWorkExp);
  };

  const addWorkExperiance = (newExperiance) => {
    const newWorkExp = [...workExperiance];
    newWorkExp.push(newExperiance);
    setWorkExperiance(newWorkExp);
  };

  const deleteWorkExperiance = (id) => {
    let foundEl = workExperiance.findIndex((el) => el.id === id);
    const newWorkExp = [...workExperiance];
    newWorkExp.splice(foundEl, 1);
    setWorkExperiance(newWorkExp);
  };

  const handleEducation = (e, id, name) => {
    const index = education.findIndex((el) => {
      return el.id === id;
    });
    const newEducation = [...education];
    newEducation[index][name] = e.target.value;
    setEducation(newEducation);
  };

  const addEducation = (newEducation) => {
    const newEducations = [...education];
    newEducations.push(newEducation);
    setEducation(newEducations);
  };
  const deleteEducation = (id) => {
    let foundEl = education.findIndex((el) => el.id === id);
    const newEducation = [...education];
    newEducation.splice(foundEl, 1);
    setEducation(newEducation);
  };

  const handleSkills = (e, id, name) => {
    const index = skills.findIndex((el) => {
      return el.id === id;
    });
    const newSkill = [...skills];
    newSkill[index][name] = e.target.value;
    setSkills(newSkill);
  };

  const addSkills = (newSkill) => {
    const newSkills = [...skills];
    newSkills.push(newSkill);
    setSkills(newSkills);
  };

  const deleteSkills = (id) => {
    let foundEl = skills.findIndex((el) => el.id === id);
    const newSkill = [...skills];
    newSkill.splice(foundEl, 1);
    setSkills(newSkill);
  };

  const handleAchievement = (e, id, name) => {
    const index = achievement.findIndex((el) => {
      return el.id === id;
    });
    const newAchievement = [...achievement];
    newAchievement[index][name] = e.target.value;
    setAchievement(newAchievement);
  };

  const addAchievement = (newAchievement) => {
    const newAchievements = [...achievement];
    newAchievements.push(newAchievement);
    setAchievement(newAchievements);
  };

  const deleteAchievement = (id) => {
    let foundEl = achievement.findIndex((el) => el.id === id);
    const newAchievement = [...achievement];
    newAchievement.splice(foundEl, 1);
    setAchievement(newAchievement);
  };

  const handleLanguages = (e, index) => {
    const { name, value, className } = e.target;
    let updated = languages.slice();
    if (name === "language") {
      updated[index].language = value;
    } else {
      updated[index].level = Number(className);
    }
    setLanguages(updated);
  };

  const addLanguages = (lan) => {
    setLanguages([...languages, lan]);
  };

  const deleteLanguages = (id) => {
    setLanguages([...languages.filter((el) => el.id !== id)]);
  };

  const handleDataScienceSkills = (e, index) => {
    const { name, value, className } = e.target;
    let updated = dataScienceSkills.slice();
    if (name === "dataScienceSkill") {
      updated[index].dataScienceSkill = value;
    } else {
      updated[index].level = Number(className);
    }
    setDataScienceSkills(updated);
  };

  const addDataScienceSkill = (lan) => {
    setDataScienceSkills([...dataScienceSkills, lan]);
  };

  const deleteDataScienceSkill = (id) => {
    setDataScienceSkills([...dataScienceSkills.filter((el) => el.id !== id)]);
  };

  const handleInformalEducation = (e, id, name) => {
    const index = informalEducations.findIndex((el) => {
      return el.id === id;
    });
    const newInformalEducation = [...informalEducations];
    newInformalEducation[index][name] = e.target.value;
    setInfromalEducation(newInformalEducation);
  };

  const addInformalEducation = (newInfo) => {
    const newInformalEducation = [...informalEducations];
    newInformalEducation.push(newInfo);
    setInfromalEducation(newInformalEducation);
  };

  const deleteInformalEducation = (id) => {
    let foundEl = informalEducations.findIndex((el) => el.id === id);
    const newInformalEducation = [...informalEducations];
    newInformalEducation.splice(foundEl, 1);
    setInfromalEducation(newInformalEducation);
  };

  const handlePdf = (cv) => {
    setPdf(cv);
  };

  const handleDownloadPdf = () => {
    window.scrollTo(0, 0);
    let opt = {
      margin: [5, -15, 5, 15],
      filename: "",
      image: { type: "png", quality: 1 },
      html2canvas: { scale: 2, useCORS: true },
      jsPDF: { unit: "mm", format: "letter" },
    };
    html2pdf().from(pdf).set(opt).save();
  };

  const handleSeenLaika = () => {
    setLaikaSeen(true);
  };

  const handleSeenLinkedIn = () => {
    setLinkedSeen(true);
  };

  const handleEditCvMode = () => {
    setEditCvMode(!editCvMode);
  };

  const usersContextObj = {
    handleTipsDisplay,
    showTips,
    basicInfo,
    handleBasicInfo,
    handleEditLocation,
    category,
    workExperiance,
    handleWorkExperiance,
    addWorkExperiance,
    deleteWorkExperiance,
    fileUpload,
    handleFileUpload,
    skills,
    handleSkills,
    addSkills,
    deleteSkills,
    education,
    handleEducation,
    addEducation,
    deleteEducation,
    achievement,
    handleAchievement,
    addAchievement,
    deleteAchievement,
    languages,
    handleLanguages,
    addLanguages,
    deleteLanguages,
    dataScienceSkills,
    handleDataScienceSkills,
    addDataScienceSkill,
    deleteDataScienceSkill,
    informalEducations,
    handleInformalEducation,
    addInformalEducation,
    deleteInformalEducation,
    handlePdf,
    handleDownloadPdf,
    isLinkedSeen,
    isLaikaSeen,
    handleSeenLaika,
    handleSeenLinkedIn,
    editCvMode,
    handleEditCvMode,
  };

  return (
    <Context.Provider value={usersContextObj}>
      {props.children}
    </Context.Provider>
  );
};
export const UserContextProvider = Provider;
export const UserContext = Context;
