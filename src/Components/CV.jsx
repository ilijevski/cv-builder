import React, { useContext } from "react";
import { UserContext } from "../useContext";
import "../CSS/CV.css";
import ElonMusk from "../Images/CV template screenshots/Web Development Resume.png";

const CV = (props) => {
  const { handleTipsDisplay } = useContext(UserContext);

  const location = props.match.path.split("/")[2];

  return (
    <div className="col cv-wapper">
      <div className="onClickDisplayWebDevelopment">
        <img className="w-100 d-block p-5" src={ElonMusk} alt="Elon Musk" />
        <div
          className="aboutMe"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="webDevPhoto"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="personalContact"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="positionCompany"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="workingExpDates"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="positionCompany extended"
          id="positionCompany"
          onClick={(e) => handleTipsDisplay(e.target.id, location)}
        ></div>
        <div
          className="skills"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
      </div>
    </div>
  );
};

export default CV;
