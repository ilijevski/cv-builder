import React, { useState, useEffect, useContext } from "react";
import Banner from "./Banner";
import "../CSS/WebDevelopment.css";
import { NavLink } from "react-router-dom";
import DisplayWrapper from "./DisplayWrapper";
import RouterSwitchComponent from "./RouterSwitchComponent";
import { UserContext } from "../useContext";

const DataScience = (props) => {
  const { handleEditLocation, editCvMode, handleEditCvMode } = useContext(
    UserContext
  );

  const [isCV, setIsCV] = useState("");

  const editLocation = props.location.pathname;

  useEffect(() => {
    setIsCV(props.location.pathname.split("/")[3]);
    handleEditLocation(editLocation);
  });
  return (
    <div className="container-fluid">
      <div className="row">
        <div md="8" className="col web-development-wrapper-left">
          <div className="web-development-button-container">
            <ul className="button-list">
              <NavLink
                to={
                  editCvMode
                    ? "/category/digitalMarketing/editDigitalMarketingCV"
                    : "/category/digitalMarketing/CV"
                }
                activeClassName="selected"
                className="button-list-item"
              >
                CV
              </NavLink>
              <NavLink
                to="/category/digitalMarketing/LinkedIn"
                activeClassName="selected"
                className="button-list-item"
              >
                LINKEDIN
              </NavLink>
              <NavLink
                to="/category/digitalMarketing/Laika"
                activeClassName="selected"
                className="button-list-item"
              >
                WEARELAIKA.COM
              </NavLink>
              {isCV === "CV" && (
                <NavLink
                  onClick={handleEditCvMode}
                  to="/category/digitalMarketing/editDigitalMarketingCV"
                  className="button-list-item-edit"
                >
                  EDIT
                </NavLink>
              )}
            </ul>
          </div>
        </div>
        <DisplayWrapper />
      </div>
      <div className="row">
        <div className="col-7">
          <RouterSwitchComponent type="digitalMarketing" />
        </div>
      </div>
      <Banner />
    </div>
  );
};

export default DataScience;
