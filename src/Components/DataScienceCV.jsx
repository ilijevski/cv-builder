import React, { useContext } from "react";
import { UserContext } from "../useContext";
import "../CSS/DataScienceCV.css";
import dataScienceCV from "../Images/CV template screenshots/Data Scientist Resume.png";

const DataScienceCV = (props) => {
  const { handleTipsDisplay } = useContext(UserContext);

  const location = props.match.path.split("/")[2];

  return (
    <div className="col cv-wapper">
      <div className="onClickDisplayDataScience">
        <img
          className="w-100 d-block p-5"
          src={dataScienceCV}
          alt="Data Science CV"
        />
        <div
          className="aboutMe"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="photo"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="socialNetworks"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="positionCompany"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="workingExpDates"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="dataScienceWorkExp"
          id="positionCompany"
          onClick={(e) => handleTipsDisplay(e.target.id, location)}
        ></div>
        <div
          className="education"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="achievements"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="languages"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="informalEducation"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
      </div>
    </div>
  );
};

export default DataScienceCV;
