import React, { useContext, useEffect } from "react";
import "../CSS/EditDataScienceCV.css";
import AddPhoto from "./CvBuilder/AddPhoto";
import BasicInfo from "./CvBuilder/BasicInfo";
import AddSocialMedia from "./CvBuilder/AddSocialMedia";
import WorkExperiance from "./CvBuilder/WorkExperiance";
import Education from "./CvBuilder/Education";
import Achievements from "./CvBuilder/Achievements";
import Languages from "./CvBuilder/Languages";
import DataScienceSkills from "./CvBuilder/DataScienceSkills";
import InformalEducation from "./CvBuilder/InformalEducation";
import Button from "./Button";
import { UserContext } from "../useContext";
import { Popover, OverlayTrigger } from "react-bootstrap";
import { Link } from "react-router-dom";

const EditDataScienceCV = () => {
  const {
    handlePdf,
    handleDownloadPdf,
    isLinkedSeen,
    isLaikaSeen,
  } = useContext(UserContext);

  const popover1 = (
    <Popover id="popover-basic">
      <Popover.Title>
        {!isLinkedSeen &&
          !isLaikaSeen &&
          "Check Linkedin and WeareLaika.com fields before downloading the CV"}
      </Popover.Title>
    </Popover>
  );

  let cv = document.querySelector("#downloadPdf");

  useEffect(() => {
    handlePdf(cv);
  });
  return (
    <div className="editDataScienceCV" id="downloadPdf">
      <div className="col cv-wapper p-5">
        <div className="row">
          <div className="col-3">
            <AddPhoto />
          </div>
          <div className="col-9">
            <BasicInfo />
          </div>
          <hr className="hrDevider" />
          <div className="row">
            <div className="col">
              <AddSocialMedia />
            </div>
          </div>
          <hr className="hrDevider" />
        </div>
        <div className="row">
          <div className="col-6 dataScienceExperiance">
            <WorkExperiance />
            <Education />
          </div>
          <div className="col-6">
            <DataScienceSkills />
            <Achievements />
            <Languages />
            <InformalEducation />
          </div>
        </div>
        <div className="row">
          <div className="col-1 offset-11">
            <OverlayTrigger trigger="focus" placement="top" overlay={popover1}>
              <Link
                className="link-to-download"
                to={isLinkedSeen && isLaikaSeen ? "/lastPage" : undefined}
              >
                <Button
                  className="home-page-button"
                  title="DOWNLOAD"
                  click={
                    isLinkedSeen && isLaikaSeen ? handleDownloadPdf : undefined
                  }
                />
              </Link>
            </OverlayTrigger>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditDataScienceCV;
