import React from "react";
import { Switch, Route } from "react-router";
import CV from "./CV";
import Laika from "./Laika";
import LinkedIn from "./LinkedIn";
import DesignCV from "./DesignCV";
import DigitalMarketingCV from "./DigitalMarketingCV";
import DataScienceCV from "./DataScienceCV";
import EditWebDevelopmentCV from "./EditWebDevelopmentCV";
import EditDataScienceCV from "./EditDataScienceCV";
import EditDesignCV from "./EditDesignCV";
import EditDigitalMarketingCV from "./EditDigitalMarketingCV";

const RouterSwitchComponent = (props) => {
  const type = props.type;
  let cvComponent;

  if (type === "webDevelopment") {
    cvComponent = CV;
  } else if (type === "design") {
    cvComponent = DesignCV;
  } else if (type === "digitalMarketing") {
    cvComponent = DigitalMarketingCV;
  } else if (type === "dataScience") {
    cvComponent = DataScienceCV;
  }
  return (
    <Switch>
      <Route path={`/category/${type}/CV`} component={cvComponent}></Route>
      <Route path={`/category/${type}/LinkedIn`} component={LinkedIn}></Route>
      <Route path={`/category/${type}/Laika`} component={Laika}></Route>
      <Route
        path={`/category/webDevelopment/editWebDevelopmentCV`}
        component={EditWebDevelopmentCV}
      ></Route>
      <Route
        path={`/category/dataScience/editDataScienceCV`}
        component={EditDataScienceCV}
      ></Route>
      <Route
        path={`/category/digitalMarketing/editDigitalMarketingCV`}
        component={EditDigitalMarketingCV}
      ></Route>
      <Route
        path={`/category/design/editDesignCV`}
        component={EditDesignCV}
      ></Route>
    </Switch>
  );
};

export default RouterSwitchComponent;
