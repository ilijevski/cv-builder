import React from "react";
import { Container, Row, Col } from "reactstrap";
import "../CSS/LastPage.css";
import Footer from "./Footer";

const LastPage = () => {
  return (
    <Container fluid={true}>
      <Row className="last-page-right-side">
        <Col sm="3">
          <h2 className="last-page-heading">
            Did you finish all three? Way to go!
          </h2>
          <p className="last-page-para">
            Good luck on your next job! If you need help, counceling or just
            want to leave a suggestion, contact us at{" "}
            <a href="mailto:hello@wearelaika.com">hello@wearelaika.com</a>.
          </p>
        </Col>
      </Row>
      <Footer />
    </Container>
  );
};

export default LastPage;
