import React, { useContext, useEffect } from "react";
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Laika1 from "../Images/Laika screenshots/Laika1.png";
import Laika2 from "../Images/Laika screenshots/Laika2.png";
import Laika3 from "../Images/Laika screenshots/laika3.png";
import Laika4 from "../Images/Laika screenshots/Laika4.png";
import Laika5 from "../Images/Laika screenshots/Laika5.png";
import Laika6 from "../Images/Laika screenshots/Laika6.png";
import { UserContext } from "../useContext";
import "../CSS/Laika.css";

const Laika = (props) => {
  const { handleTipsDisplay, handleSeenLaika } = useContext(UserContext);
  useEffect(() => {
    handleSeenLaika();
  });

  const location = props.match.path.split("/")[3];

  return (
    <div className="col-10 offset-1">
      <Carousel className="text-center pb-5">
        <Carousel.Item>
          <img
            className="w-75"
            src={Laika1}
            alt="Laika1"
            name="LaikaEmail"
            onMouseEnter={(e) => handleTipsDisplay(e.target.name, location)}
          />
        </Carousel.Item>
        <Carousel.Item>
          <div className="split-field">
            <img className="w-75" src={Laika2} alt="Laika2" />
            <div
              className="split-field-top-half"
              id="LaikaSocialMedia"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
            <div
              className="split-field-bottom-half"
              id="LaikaOpertunity"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="split-field">
            <img className="w-75" src={Laika3} alt="Laika3" />
            <div
              className="split-field-first-quarter"
              id="LaikaIndustry"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
            <div
              className="split-field-second-quarter"
              id="LaikaExpertise"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>{" "}
            <div
              className="split-field-thrdth-quarter"
              id="LaikaTechnologies"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
            <div
              className="split-field-fourth-quarter"
              id="LaikaWorkExperiance"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="w-75"
            src={Laika4}
            alt="Laika4"
            name="LaikaEducation"
            onMouseEnter={(e) => handleTipsDisplay(e.target.name, location)}
          />
        </Carousel.Item>
        <Carousel.Item>
          <div className="split-field max-height">
            <img className="w-75" src={Laika5} alt="Laika5" />
            <div
              className="split-field-first-quarter"
              id="LaikaLocations"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
            <div
              className="split-field-second-quarter"
              id="LaikaJobType"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>{" "}
            <div
              className="split-field-thrdth-quarter"
              id="LaikaJobTitle"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
            <div
              className="split-field-fourth-quarter"
              id="LaikaOpportunity"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="split-field">
            <img className="w-75" src={Laika6} alt="Laika5" />
            <div
              className="split-field-first-thrdth"
              id="LaikaExperienceLevel"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
            <div
              className="split-field-second-thrdth"
              id="LaikaSalary"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>{" "}
            <div
              className="split-field-thrdth-thrdth"
              id="LaikaJobPlan"
              onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
            ></div>
          </div>
        </Carousel.Item>
      </Carousel>
    </div>
  );
};

export default Laika;
