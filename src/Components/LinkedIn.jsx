import React, { useContext, useEffect } from "react";
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../CSS/LinkedIn.css";
import bill01 from "../Images/LinkedIn/bill1.png";
import bill02 from "../Images/LinkedIn/bill2.png";
import bill03 from "../Images/LinkedIn/bill3.png";
import bill04 from "../Images/LinkedIn/bill4.png";
import { UserContext } from "../useContext";

const LinkedIn = (props) => {
  const { handleTipsDisplay, handleSeenLinkedIn } = useContext(UserContext);
  useEffect(() => {
    handleSeenLinkedIn();
  });

  const location = props.match.path.split("/")[3];

  return (
    <div className="row">
      <div className="col-10 offset-1">
        <Carousel className="text-center py-5">
          <Carousel.Item>
            <img
              className="w-75"
              src={bill01}
              alt="bill01"
              name="LinkedInAbout"
              onMouseEnter={(e) => handleTipsDisplay(e.target.name, location)}
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="w-75"
              src={bill02}
              alt="bill02"
              name="LinkedInArticles"
              onMouseEnter={(e) => handleTipsDisplay(e.target.name, location)}
            />
          </Carousel.Item>
          <Carousel.Item>
            <div className="split-field-half">
              <img className="w-75" src={bill03} alt="bill03" />
              <div
                className="split-field-three-one"
                id="LinkedInExperiance"
                onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
              ></div>
              <div
                className="split-field-three-two"
                id="LinkedInEducation"
                onMouseEnter={(e) => handleTipsDisplay(e.target.id, location)}
              ></div>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="w-75"
              src={bill04}
              alt="bill5"
              name="LinkedInInterests"
              onMouseEnter={(e) => handleTipsDisplay(e.target.name, location)}
            />
          </Carousel.Item>
        </Carousel>
      </div>
    </div>
  );
};

export default LinkedIn;
