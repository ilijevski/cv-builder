import React, { useContext } from "react";
import "../CSS/DisplayWrapper.css";
import { UserContext } from "../useContext";

const DisplayWrapper = () => {
  const { showTips } = useContext(UserContext);

  return (
    <div className="col-4 display-wapper">
      <div className="display-tips">{showTips}</div>
    </div>
  );
};

export default DisplayWrapper;
