import React, { useContext } from "react";
import { UserContext } from "../useContext";
import digitalMarketing from "../Images/CV template screenshots/Digital Marketing Resume.png";
import "../CSS/DigitalMarketingCV.css";

const DigitalMarketingCV = (props) => {
  const { handleTipsDisplay } = useContext(UserContext);

  const location = props.match.path.split("/")[2];

  return (
    <div className="col cv-wapper">
      <div className="onClickDisplayDigitalMarketing">
        <img
          className="w-100 d-block p-5"
          src={digitalMarketing}
          alt="digital marketing"
        />
        <div
          className="aboutMe"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="photo"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="socialNetworks"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="positionCompany"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="workingExpDates"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="positionCompany"
          id="digitalMarketingWorkExp"
          onClick={(e) => handleTipsDisplay(e.target.id, location)}
        ></div>
        <div
          className="education"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="skills"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="languages"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
      </div>
    </div>
  );
};

export default DigitalMarketingCV;
