import React from "react";
import "../CSS/Button.css";

function Button(props) {
  return (
    <div>
      <button
        data-html2canvas-ignore
        className="home-page-button"
        onClick={props.click}
      >
        {props.title}
      </button>
    </div>
  );
}

export default Button;
