import React, { useContext, useEffect } from "react";
import "../CSS/EditDigitalMarketingCV.css";
import Achievements from "./CvBuilder/Achievements";
import AddPhoto from "./CvBuilder/AddPhoto";
import AddSocialMedia from "./CvBuilder/AddSocialMedia";
import BasicInfo from "./CvBuilder/BasicInfo";
import Education from "./CvBuilder/Education";
import Languages from "./CvBuilder/Languages";
import Skills from "./CvBuilder/Skills";
import WorkExperiance from "./CvBuilder/WorkExperiance";
import Button from "./Button";
import { UserContext } from "../useContext";
import { Popover, OverlayTrigger } from "react-bootstrap";
import { Link } from "react-router-dom";

const EditDigitalMarketingCV = () => {
  const {
    handlePdf,
    handleDownloadPdf,
    isLinkedSeen,
    isLaikaSeen,
  } = useContext(UserContext);

  const popover1 = (
    <Popover id="popover-basic">
      <Popover.Title>
        {!isLinkedSeen &&
          !isLaikaSeen &&
          "Check Linkedin and WeareLaika.com fields before downloading the CV"}
      </Popover.Title>
    </Popover>
  );

  let cv = document.querySelector("#downloadPdf");

  useEffect(() => {
    handlePdf(cv);
  });
  return (
    <div className="editDigitalMarketing" id="downloadPdf">
      <div className="cv-wapper p-5">
        <div className="col-3">
          <AddPhoto />
        </div>
        <div className="col-9">
          <BasicInfo />
        </div>
        <div className="row">
          <div className="col socialMediaBar">
            <AddSocialMedia />
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <WorkExperiance />
            <Education />
          </div>
          <div className="col-6">
            <Skills />
            <Achievements />
            <Languages />
          </div>
        </div>
        <div className="row">
          <div className="col-1 offset-11">
            <OverlayTrigger trigger="focus" placement="top" overlay={popover1}>
              <Link
                className="link-to-download"
                to={isLinkedSeen && isLaikaSeen ? "/lastPage" : undefined}
              >
                <Button
                  className="home-page-button"
                  title="DOWNLOAD"
                  click={
                    isLinkedSeen && isLaikaSeen ? handleDownloadPdf : undefined
                  }
                />
              </Link>
            </OverlayTrigger>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditDigitalMarketingCV;
