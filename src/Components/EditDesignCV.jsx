import React, { useContext, useEffect } from "react";
import "../CSS/EditDesignCV.css";
import BasicInfo from "./CvBuilder/BasicInfo";
import WorkExperiance from "./CvBuilder/WorkExperiance";
import AddSocalMedia from "./CvBuilder/AddSocialMedia";
import Education from "./CvBuilder/Education";
import Skills from "./CvBuilder/Skills";
import Button from "./Button";
import { UserContext } from "../useContext";
import { Popover, OverlayTrigger } from "react-bootstrap";
import { Link } from "react-router-dom";

const EditDesignCV = () => {
  const {
    handlePdf,
    handleDownloadPdf,
    isLinkedSeen,
    isLaikaSeen,
  } = useContext(UserContext);

  const popover1 = (
    <Popover id="popover-basic">
      <Popover.Title>
        {!isLinkedSeen &&
          !isLaikaSeen &&
          "Check Linkedin and WeareLaika.com fields before downloading the CV"}
      </Popover.Title>
    </Popover>
  );

  let cv = document.querySelector("#downloadPdf");

  useEffect(() => {
    handlePdf(cv);
  });
  return (
    <div className="editDesignCV" id="downloadPdf">
      <div className="cv-wapper p-5">
        <div className="row">
          <div className="col border">
            <div className="row">
              <div className="col basicInfo">
                <BasicInfo />
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <WorkExperiance />
                <AddSocalMedia />
              </div>
              <div className="col-6">
                <Education />
                <Skills />
              </div>
            </div>
            <div className="row">
              <div className="col-1 offset-11">
                <OverlayTrigger
                  trigger="focus"
                  placement="top"
                  overlay={popover1}
                >
                  <Link
                    className="link-to-download"
                    to={isLinkedSeen && isLaikaSeen ? "/lastPage" : undefined}
                  >
                    <Button
                      className="home-page-button"
                      title="DOWNLOAD"
                      click={
                        isLinkedSeen && isLaikaSeen
                          ? handleDownloadPdf
                          : undefined
                      }
                    />
                  </Link>
                </OverlayTrigger>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditDesignCV;
