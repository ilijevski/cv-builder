import React from "react";
import { Row, Col } from "reactstrap";
import "../CSS/Banner.css";

const Banner = () => {
  return (
    <Row className="banner-position">
      <Col className="left-side">
        <p className="banner-text">
          Do you want to learn hand-on digital skills?
        </p>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://codepreneurs.brainster.co/"
        >
          <button className="banner-button">VISIT BRAINSTER</button>
        </a>
      </Col>
      <Col className="right-side">
        <p className="banner-text">
          Do you want to recive job offers by IT Companies?
        </p>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.wearelaika.com/"
        >
          <button className="banner-button">VISIT LAIKA</button>
        </a>
      </Col>
    </Row>
  );
};

export default Banner;
