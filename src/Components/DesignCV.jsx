import React, { useContext } from "react";
import { UserContext } from "../useContext";
import "../CSS/DesignCV.css";
import design from "../Images/CV template screenshots/Graphic Design Resume.png";

const DesignCV = (props) => {
  const { handleTipsDisplay } = useContext(UserContext);

  const location = props.match.path.split("/")[2];

  return (
    <div className="col cv-wapper">
      <div className="onClickDisplayDesign">
        <img className="w-100 d-block p-5" src={design} alt="Elon Musk" />
        <div
          className="designAboutMe"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="designWorkExperiance"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="designSocialMedia"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="designEducation"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
        <div
          className="designSkills"
          onClick={(e) => handleTipsDisplay(e.target.className, location)}
        ></div>
      </div>
    </div>
  );
};

export default DesignCV;
