import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import Button from "./Button";
import Footer from "./Footer";
import "../CSS/Home.css";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className="container-fluid">
      <div className="row main-section">
        <div className="col-md-4 offset-md-2 main-section-left">
          <h1 className="headline">
            The Ultimate <br /> CV & Portfolio Check - List
          </h1>
          <p className="main-para">
            Are you a Web Developer, Data Scientist, Digital Marketer, or a
            Designer? Have your CV and portfolio in check and create a 5-star
            representation of your skills with this guide.
          </p>
          <Link to="/category">
            <Button title="STEP INSIDE" />
          </Link>
        </div>
        <div className="col-6 right-side-bg-image"></div>
      </div>
      <Footer />
    </div>
  );
};

export default Home;
