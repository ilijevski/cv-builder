import React from "react";
import { Row, Col } from "reactstrap";
import "../CSS/Footer.css";

const Footer = () => {
  return (
    <Row>
      <Col>
        <footer className="home-footer">
          <p>
            Created with {"<3"} by the{" "}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://codepreneurs.brainster.co/"
              className="footer-link-hover"
            >
              Brainster Coding Academy
            </a>{" "}
            students and{" "}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.wearelaika.com/"
            >
              wearelaika.com
            </a>
          </p>
        </footer>
      </Col>
    </Row>
  );
};

export default Footer;
