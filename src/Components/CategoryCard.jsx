import React from "react";
import { Card, CardBody, CardTitle } from "reactstrap";
import "../CSS/CategoryCard.css";

const ChooseCard = (props) => {
  return (
    <Card className="card-design">
      <CardBody>
        <CardTitle className="card-title" tag="h5">
          {props.cardTitle}
        </CardTitle>
        <div className="position-card-button">{props.children}</div>
      </CardBody>
    </Card>
  );
};

export default ChooseCard;
