import React, { useContext } from "react";
import "./WorkExperiance.css";
import { UserContext } from "../../useContext";
import { v4 as uuid } from "uuid";

const WorkExperiance = (props) => {
  const {
    category,
    workExperiance,
    handleWorkExperiance,
    handleTipsDisplay,
    addWorkExperiance,
    deleteWorkExperiance,
  } = useContext(UserContext);

  return (
    <div className="row">
      {/* <div className="col-2">
        <li className="start-devision2 mt-5"></li>
      </div> */}
      <div className="col-12 mt-3 mb-2">
        <h4 className="header">WORK EXPERIENCE</h4>
        <span>
          <button
            className="fn-button"
            onClick={() =>
              addWorkExperiance({
                id: uuid(),
                title: "",
                company: "",
                accomplishments: "",
                accomplishment1: "",
                task1: "",
                task2: "",
                startDate: "",
                endDate: "",
              })
            }
          >
            <i class="fas fa-plus"></i>
          </button>
        </span>
        {workExperiance &&
          workExperiance.map((el) => (
            <div>
              <span>
                <button
                  className="fn-button"
                  onClick={() => {
                    deleteWorkExperiance(el.id);
                  }}
                >
                  <i class="fas fa-times"></i>
                </button>
              </span>
              <input
                className="h5 input-style-first shorter"
                type="text"
                placeholder="Position/Title"
                name="title"
                value={el.title}
                onChange={(e) => handleWorkExperiance(e, el.id, e.target.name)}
                id="positionCompany"
                onClick={(e) => handleTipsDisplay(e.target.id, category)}
              />
              <input
                className="h5 input-style"
                type="text"
                placeholder="Company/Workplace"
                name="company"
                value={el.company}
                onChange={(e) => handleWorkExperiance(e, el.id, e.target.name)}
              />
              <br />
              <input
                type="date"
                className="input-date"
                value={el.startDate}
                name="startDate"
                id="workingExpDates"
                onChange={(e) => handleWorkExperiance(e, el.id, e.target.name)}
                onClick={(e) => handleTipsDisplay(e.target.id, category)}
              />
              <span>/</span>
              <input
                type="date"
                className="input-date"
                value={el.endDate}
                name="endDate"
                id="workingExpDates"
                onChange={(e) => handleWorkExperiance(e, el.id, e.target.name)}
                onClick={(e) => handleTipsDisplay(e.target.id, category)}
              />
              <br />
              <input
                className="input-style"
                type="text"
                placeholder="Accomplishments/Tasks/Duties (Optional)"
                name="accomplishments"
                value={el.accomplishments}
                onChange={(e) => handleWorkExperiance(e, el.id, e.target.name)}
              />
              <ul>
                <li className="costum-list-style-type">
                  <input
                    className="input-style"
                    type="text"
                    placeholder="Accomplishment1"
                    name="accomplishment1"
                    id="positionCompany"
                    value={el.accomplishment1}
                    onClick={(e) => handleTipsDisplay(e.target.id, category)}
                    onChange={(e) =>
                      handleWorkExperiance(e, el.id, e.target.name)
                    }
                  />
                </li>
                <li className="costum-list-style-type">
                  <input
                    className="input-style"
                    type="text"
                    placeholder="Task1"
                    name="task1"
                    value={el.task1}
                    onChange={(e) =>
                      handleWorkExperiance(e, el.id, e.target.name)
                    }
                  />
                </li>
                <li className="costum-list-style-type">
                  <input
                    className="input-style"
                    type="text"
                    placeholder="Task2"
                    name="task2"
                    value={el.task2}
                    onChange={(e) =>
                      handleWorkExperiance(e, el.id, e.target.name)
                    }
                  />
                </li>
              </ul>
            </div>
          ))}
      </div>
    </div>
  );
};

export default WorkExperiance;
