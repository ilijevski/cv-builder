import React, { useContext, useState } from "react";
import { UserContext } from "../../useContext";
import { v4 as uuid } from "uuid";

const DataScienceSkills = () => {
  const {
    category,
    dataScienceSkills,
    handleDataScienceSkills,
    addDataScienceSkill,
    deleteDataScienceSkill,
    handleTipsDisplay,
  } = useContext(UserContext);

  const [focused, setFocused] = useState(true);
  return (
    <div>
      <div>
        <h4 className="header mt-3 mb-2">SKILLS</h4>

        {dataScienceSkills &&
          dataScienceSkills.map((element, index) => (
            <div className="form-wrapper" key={index}>
              <div>
                <input
                  className="dataScienceSkill"
                  type="text"
                  value={element.dataScienceSkill}
                  onChange={(e) => handleDataScienceSkills(e, index)}
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  name="dataScienceSkill"
                  onClick={(e) =>
                    handleTipsDisplay(e.target.className, category)
                  }
                />
                {focused && (
                  <span>
                    <span>
                      <button
                        className="fn-button"
                        onClick={() =>
                          addDataScienceSkill({
                            id: uuid(),
                            dataScienceSkill: "",
                            level: "",
                          })
                        }
                      >
                        <i class="fas fa-plus"></i>
                      </button>
                    </span>
                    <button
                      className="fn-button"
                      onClick={() => {
                        deleteDataScienceSkill(element.id);
                      }}
                    >
                      <i class="fas fa-times"></i>
                    </button>
                  </span>
                )}
              </div>
              {!focused ? (
                <input
                  type="text"
                  readOnly
                  onFocus={() => setFocused(true)}
                  className="dataScienceSkill"
                  value={
                    element.level === 1
                      ? "Trainee"
                      : element.level === 2
                      ? "Junior Developer"
                      : element.level === 3
                      ? "Middle Developer"
                      : element.level === 4
                      ? "Senior Developer"
                      : "Architect"
                  }
                />
              ) : (
                <div className="language-level">
                  <span
                    onMouseDown={(e) => handleDataScienceSkills(e, index)}
                    className="1"
                    onClick={() => setFocused(false)}
                  >
                    1/5
                  </span>
                  <span
                    onMouseDown={(e) => handleDataScienceSkills(e, index)}
                    className="2"
                    onClick={() => setFocused(false)}
                  >
                    2/5
                  </span>
                  <span
                    onMouseDown={(e) => handleDataScienceSkills(e, index)}
                    className="3"
                    onClick={() => setFocused(false)}
                  >
                    3/5
                  </span>
                  <span
                    onMouseDown={(e) => handleDataScienceSkills(e, index)}
                    className="4"
                    onClick={() => setFocused(false)}
                  >
                    4/5
                  </span>
                  <span
                    onMouseDown={(e) => handleDataScienceSkills(e, index)}
                    className="5"
                    onClick={() => setFocused(false)}
                  >
                    5/5
                  </span>
                </div>
              )}
            </div>
          ))}
      </div>
    </div>
  );
};

export default DataScienceSkills;
