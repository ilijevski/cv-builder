import React, { useContext } from "react";
import { UserContext } from "../../useContext";
import "./BasicInfo.css";

const BasicInfo = (props) => {
  const {
    handleTipsDisplay,
    basicInfo,
    handleBasicInfo,
    category,
  } = useContext(UserContext);

  return (
    <div className="row">
      {/* <div className="col-2">
        <li className="start-devision"></li>
      </div> */}
      <div className="col-10">
        <input
          className="h4 input-style"
          type="text"
          placeholder="Full Name"
          value={basicInfo.name}
          name="name"
          onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
        />
        <input
          className="input-style"
          type="text"
          placeholder="Professional title"
          value={basicInfo.profession}
          name="profession"
          onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
        />
        <textarea
          className="input-style"
          type="text"
          placeholder="About me"
          name="aboutMe"
          value={basicInfo.aboutMe}
          onClick={(e) => handleTipsDisplay(e.target.name, category)}
          onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
        />
      </div>
    </div>
  );
};

export default BasicInfo;
