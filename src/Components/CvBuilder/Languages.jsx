import React, { useContext, useState } from "react";
import { UserContext } from "../../useContext";
import { v4 as uuid } from "uuid";
import "./Languages.css";

const Languages = (props) => {
  const {
    category,
    languages,
    handleLanguages,
    addLanguages,
    deleteLanguages,
    handleTipsDisplay,
  } = useContext(UserContext);

  const [focused, setFocused] = useState(true);

  return (
    <>
      <div>
        <h4 className="header mt-5 mb-2">LANGUAGES</h4>

        {languages &&
          languages.map((element, index) => (
            <div className="form-wrapper" key={index}>
              <div>
                <input
                  className="languages"
                  type="text"
                  value={element.language}
                  onChange={(e) => handleLanguages(e, index)}
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  name="language"
                  placeholder="Language"
                  onClick={(e) =>
                    handleTipsDisplay(e.target.className, category)
                  }
                />
                {focused && (
                  <span>
                    <span>
                      <button
                        className="fn-button"
                        onClick={() => {
                          deleteLanguages(element.id);
                        }}
                      >
                        <i class="fas fa-times"></i>
                      </button>
                    </span>
                    <span>
                      <button
                        className="fn-button"
                        onClick={() =>
                          addLanguages({
                            id: uuid(),
                            language: "",
                            level: "",
                          })
                        }
                      >
                        <i class="fas fa-plus"></i>
                      </button>
                    </span>
                  </span>
                )}
              </div>
              {!focused ? (
                <input
                  type="text"
                  readOnly
                  onFocus={() => setFocused(true)}
                  className="language-level"
                  value={
                    element.level === 1
                      ? "Elementary Proficiency"
                      : element.level === 2
                      ? "Limited Working Proficiency"
                      : element.level === 3
                      ? "Professional Working Proficiency"
                      : element.level === 4
                      ? "Full Professional Proficiency"
                      : "Native or Bilingual Proficiency"
                  }
                />
              ) : (
                <div className="language-level">
                  <span
                    onMouseDown={(e) => handleLanguages(e, index)}
                    className="1"
                    onClick={() => setFocused(false)}
                  >
                    1/5
                  </span>
                  <span
                    onMouseDown={(e) => handleLanguages(e, index)}
                    className="2"
                    onClick={() => setFocused(false)}
                  >
                    2/5
                  </span>
                  <span
                    onMouseDown={(e) => handleLanguages(e, index)}
                    className="3"
                    onClick={() => setFocused(false)}
                  >
                    3/5
                  </span>
                  <span
                    onMouseDown={(e) => handleLanguages(e, index)}
                    className="4"
                    onClick={() => setFocused(false)}
                  >
                    4/5
                  </span>
                  <span
                    onMouseDown={(e) => handleLanguages(e, index)}
                    className="5"
                    onClick={() => setFocused(false)}
                  >
                    5/5
                  </span>
                </div>
              )}
            </div>
          ))}
      </div>
    </>
  );
};
export default Languages;
