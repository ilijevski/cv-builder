import React, { useContext } from "react";
import { UserContext } from "../../useContext";
import { v4 as uuid } from "uuid";
import "./Achievements.css";

const Achievements = () => {
  const {
    achievement,
    handleAchievement,
    handleTipsDisplay,
    category,
    addAchievement,
    deleteAchievement,
  } = useContext(UserContext);
  return (
    <>
      <h4 className="header mt-3 mb-2">ACHIEVEMENTS & CERTIFICATES</h4>

      {achievement &&
        achievement.map((el) => (
          <div>
            <span>
              <button
                className="fn-button"
                onClick={() =>
                  addAchievement({
                    id: uuid(),
                    achievement: "",
                    achievementDesc: "",
                  })
                }
              >
                <i class="fas fa-plus"></i>
              </button>
              <button
                className="fn-button"
                onClick={() => {
                  deleteAchievement(el.id);
                }}
              >
                <i class="fas fa-times"></i>
              </button>
            </span>
            <input
              className="input-style-first shorter"
              type="text"
              placeholder="Achievement"
              name="achievements"
              id="achievements"
              onChange={(e) => handleAchievement(e, el.id, e.target.name)}
              onClick={(e) => handleTipsDisplay(e.target.name, category)}
            />
            <br />
            <input
              className="input-style"
              type="text"
              placeholder="If needed write here description about achievment"
              name="achievementDesc"
              id="achievementDesc"
              onChange={(e) => handleAchievement(e, el.id, e.target.name)}
            />
          </div>
        ))}
    </>
  );
};

export default Achievements;
