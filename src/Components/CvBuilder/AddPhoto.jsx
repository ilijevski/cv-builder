import React, { useContext } from "react";
import { UserContext } from "../../useContext";

const AddPhoto = (props) => {
  const {
    handleTipsDisplay,
    basicInfo,
    handleBasicInfo,
    category,
    fileUpload,
    handleFileUpload,
  } = useContext(UserContext);
  return (
    <>
      <div className="img-upload">
        <label for="file-input">
          <img className="img-file-input" src={fileUpload} alt="addImg" />
        </label>
        <input
          type="file"
          id="file-input"
          name="photo"
          value={basicInfo.webDevPhoto}
          onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
          onClick={(e) => {
            handleTipsDisplay(e.target.name, category);
            handleFileUpload(e);
          }}
        />
      </div>
    </>
  );
};

export default AddPhoto;
