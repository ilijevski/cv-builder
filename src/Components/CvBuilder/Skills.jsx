import React, { useContext } from "react";
import { UserContext } from "../../useContext";
import { v4 as uuid } from "uuid";
import "./Skills.css";

const Skills = (props) => {
  const {
    category,
    handleSkills,
    handleTipsDisplay,
    skills,
    addSkills,
    deleteSkills,
  } = useContext(UserContext);

  return (
    <>
      <div className="skills-container mt-3">
        <h4 className="header">SKILLS & COMPETENCIES</h4>

        {skills &&
          skills.map((el) => (
            <div>
              <span>
                <input
                  className="input-style-first shorter"
                  type="text"
                  placeholder="Skill"
                  name="skill"
                  id="skills"
                  onChange={(e) => handleSkills(e, el.id, e.target.name)}
                  onClick={(e) => handleTipsDisplay(e.target.id, category)}
                />
              </span>
              <span>
                <button
                  className="fn-button"
                  onClick={() =>
                    addSkills({
                      id: uuid(),
                      skill: "",
                    })
                  }
                >
                  <i class="fas fa-plus"></i>
                </button>
                <button
                  className="fn-button"
                  onClick={() => {
                    deleteSkills(el.id);
                  }}
                >
                  <i class="fas fa-times"></i>
                </button>
              </span>
            </div>
          ))}
      </div>
    </>
  );
};

export default Skills;
