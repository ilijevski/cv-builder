import React, { useContext } from "react";
import { UserContext } from "../../useContext";
import { v4 as uuid } from "uuid";

const InformalEducation = () => {
  const {
    category,
    handleTipsDisplay,
    informalEducations,
    handleInformalEducation,
    addInformalEducation,
    deleteInformalEducation,
  } = useContext(UserContext);
  return (
    <div>
      <div className="informal-education-container">
        <h4 className="header">INFORMAL EDUCATION</h4>

        {informalEducations &&
          informalEducations.map((el) => (
            <div>
              <span>
                <input
                  className="input-style-first informal-education"
                  type="text"
                  placeholder="Course"
                  name="informalEducation"
                  id="informalEducations"
                  onChange={(e) =>
                    handleInformalEducation(e, el.id, e.target.name)
                  }
                  onClick={(e) => handleTipsDisplay(e.target.id, category)}
                />
              </span>
              <span>
                <button
                  className="fn-button"
                  onClick={() =>
                    addInformalEducation({
                      id: uuid(),
                      informalEducation: "",
                    })
                  }
                >
                  <i class="fas fa-plus"></i>
                </button>
              </span>
              <span>
                <button
                  className="fn-button"
                  onClick={() => {
                    deleteInformalEducation(el.id);
                  }}
                >
                  <i class="fas fa-times"></i>
                </button>
              </span>
            </div>
          ))}
      </div>
    </div>
  );
};

export default InformalEducation;
