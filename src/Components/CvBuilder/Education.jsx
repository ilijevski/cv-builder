import React, { useContext } from "react";
import { UserContext } from "../../useContext";
import { v4 as uuid } from "uuid";
import "./Education.css";

const Education = () => {
  const {
    category,
    education,
    handleEducation,
    handleTipsDisplay,
    addEducation,
    deleteEducation,
  } = useContext(UserContext);

  return (
    <div className="row">
      {/* <div className="col-2">
        <li className="start-devision2 mt-5"></li>
      </div> */}
      <div className="col-10 mt-3">
        <h4 className="header">EDUCATION</h4>
        <span>
          <button
            className="fn-button"
            onClick={() =>
              addEducation({
                id: uuid(),
                studyProgram: "",
                institution: "",
                courses: "",
                thingLearned1: "",
                thingLearned2: "",
                thingLearned3: "",
              })
            }
          >
            <i class="fas fa-plus"></i>
          </button>
        </span>
        {education &&
          education.map((el) => (
            <div>
              <span>
                <button
                  className="fn-button"
                  onClick={() => {
                    deleteEducation(el.id);
                  }}
                >
                  <i class="fas fa-times"></i>
                </button>
              </span>
              <input
                className="input-style-first shorter"
                type="text"
                name="positionCompany"
                placeholder="Study Program"
                id="studyProgram"
                onChange={(e) => handleEducation(e, el.id, e.target.name)}
                onClick={(e) => handleTipsDisplay(e.target.name, category)}
              />
              <input
                className="input-style"
                type="text"
                name="positionCompany"
                placeholder="Institution"
                id=""
                onChange={(e) => handleEducation(e, el.id, e.target.name)}
                onClick={(e) => handleTipsDisplay(e.target.name, category)}
              />
              <input
                className="input-date"
                type="date"
                name="workingExpDates"
                onChange={(e) => handleEducation(e, el.id, e.target.name)}
                onClick={(e) => handleTipsDisplay(e.target.name, category)}
              />
              <span>/</span>
              <input
                className="input-date"
                type="date"
                name="workingExpDates"
                onChange={(e) => handleEducation(e, el.id, e.target.name)}
                onClick={(e) => handleTipsDisplay(e.target.name, category)}
              />
              <input
                className="input-style"
                type="text"
                name="positionCompany"
                placeholder="Studied/Courses"
                onChange={(e) => handleEducation(e, el.id, e.target.name)}
                onClick={(e) => handleTipsDisplay(e.target.name, category)}
              />
              <ul>
                <li className="costum-list-style-type">
                  <input
                    className="input-style"
                    type="text"
                    name="positionCompany"
                    placeholder="Thing Learned 1"
                    onChange={(e) => handleEducation(e, el.id, e.target.name)}
                    onClick={(e) => handleTipsDisplay(e.target.name, category)}
                  />
                </li>
                <li className="costum-list-style-type">
                  <input
                    className="input-style"
                    type="text"
                    name="positionCompany"
                    placeholder="Thing Learned 2"
                    onChange={(e) => handleEducation(e, el.id, e.target.name)}
                    onClick={(e) => handleTipsDisplay(e.target.name, category)}
                  />
                </li>
                <li className="costum-list-style-type">
                  <input
                    className="input-style"
                    type="text"
                    name="positionCompany"
                    placeholder="Thing Learned 3"
                    onChange={(e) => handleEducation(e, el.id, e.target.name)}
                    onClick={(e) => handleTipsDisplay(e.target.name, category)}
                  />
                </li>
              </ul>
            </div>
          ))}
      </div>
    </div>
  );
};

export default Education;
