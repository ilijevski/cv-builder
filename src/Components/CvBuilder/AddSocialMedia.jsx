import React, { useContext } from "react";
import { UserContext } from "../../useContext";

const AddSocialMedia = (props) => {
  const {
    handleTipsDisplay,
    basicInfo,
    handleBasicInfo,
    category,
  } = useContext(UserContext);
  return (
    <>
      <input
        className="input-style-first data-science"
        type="email"
        placeholder="Email"
        id="socialNetworks"
        name="email"
        value={basicInfo.email}
        onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
        onClick={(e) => handleTipsDisplay(e.target.id, category)}
      />
      <span>
        <i class="far fa-envelope"></i>
      </span>
      <input
        className="input-style-first"
        type="text"
        placeholder="Phone Number"
        value={basicInfo.phoneNumber}
        id="socialNetworks"
        name="phoneNumber"
        onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
        onClick={(e) => handleTipsDisplay(e.target.id, category)}
      />
      <span>
        <i class="fas fa-mobile-alt"></i>
      </span>
      <input
        className="input-style-first"
        type="text"
        placeholder="City, Country"
        value={basicInfo.city}
        id="socialNetworks"
        name="city"
        onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
        onClick={(e) => handleTipsDisplay(e.target.id, category)}
      />
      <span>
        <i class="fas fa-map-marker-alt"></i>
      </span>
      <input
        className="input-style-first"
        type="text"
        placeholder="Twitter"
        id="socialNetworks"
        name="twitter"
        value={basicInfo.twitter}
        onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
        onClick={(e) => handleTipsDisplay(e.target.id, category)}
      />
      <span>
        <i class="fab fa-twitter"></i>
      </span>
      <input
        className="input-style-first"
        type="text"
        placeholder="Linkedin"
        id="socialNetworks"
        name="linkedIn"
        value={basicInfo.linkedIn}
        onChange={(e) => handleBasicInfo(e.target.value, e.target.name)}
        onClick={(e) => handleTipsDisplay(e.target.id, category)}
      />
      <span>
        <i class="fab fa-linkedin"></i>
      </span>
    </>
  );
};

export default AddSocialMedia;
