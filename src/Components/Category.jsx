import React from "react";
import { Container, Row, Col } from "reactstrap";
import Banner from "./Banner";
import Button from "./Button";
import CategoryCard from "./CategoryCard";
import "../CSS/Category.css";
import { Link } from "react-router-dom";

const Choose = () => {
  return (
    <Container className="container-fluid" fluid={true}>
      <Row>
        <Col className="header-section">
          <h1 className="header">Choose your category</h1>
        </Col>
      </Row>
      <Row className="main-section-choose">
        <Col>
          <CategoryCard cardTitle="Web Development">
            <Link to="/category/webDevelopment">
              <Button title="CHOOSE" />
            </Link>
          </CategoryCard>
        </Col>
        <Col>
          <CategoryCard cardTitle="Data Science">
            <Link to="/category/dataScience">
              <Button title="CHOOSE" />
            </Link>
          </CategoryCard>
        </Col>
        <Col>
          <CategoryCard cardTitle="Digital Marketing">
            <Link to="/category/digitalMarketing">
              <Button title="CHOOSE" />
            </Link>
          </CategoryCard>
        </Col>
        <Col>
          <CategoryCard cardTitle="Design">
            <Link to="/category/design">
              <Button title="CHOOSE" />
            </Link>
          </CategoryCard>
        </Col>
      </Row>
      <Banner />
    </Container>
  );
};

export default Choose;
