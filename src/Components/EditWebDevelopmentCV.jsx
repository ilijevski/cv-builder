import React, { useContext, useEffect } from "react";
import "../CSS/EditWebDevelopmentCV.css";
import BasicInfo from "./CvBuilder/BasicInfo";
import Skills from "./CvBuilder/Skills";
import Education from "./CvBuilder/Education";
import WorkExperiance from "./CvBuilder/WorkExperiance";
import Achievements from "./CvBuilder/Achievements";
import Languages from "./CvBuilder/Languages";
import Button from "./Button";
import AddPhoto from "./CvBuilder/AddPhoto";
import AddSocialMedia from "./CvBuilder/AddSocialMedia";
import StartDevision from "./CvBuilder/StartDevision";
import { UserContext } from "../useContext";
import { Link } from "react-router-dom";
import { Popover, OverlayTrigger } from "react-bootstrap";

const EditWebDevelopmentCV = () => {
  const {
    handlePdf,
    handleDownloadPdf,
    isLinkedSeen,
    isLaikaSeen,
  } = useContext(UserContext);

  const popover1 = (
    <Popover id="popover-basic">
      <Popover.Title>
        {!isLinkedSeen &&
          !isLaikaSeen &&
          "Check Linkedin and WeareLaika.com fields before downloading the CV"}
      </Popover.Title>
    </Popover>
  );

  let cv = document.querySelector("#downloadPdf");

  useEffect(() => {
    handlePdf(cv);
  });

  return (
    <div className="col cv-wapper p-5 webDevelopment" id="downloadPdf">
      <div className="row">
        <div className="col-1">
          <StartDevision />
        </div>
        <div className="col-3">
          <BasicInfo />
        </div>
        <div className="col-4">
          <AddPhoto />
        </div>
        <div className="col-4">
          <AddSocialMedia />
        </div>
      </div>
      <hr className="devider" />
      <div className="row">
        <div className="col-1 start-devision-blue">
          <StartDevision />
        </div>
        <div className="col-5">
          <WorkExperiance />
        </div>
        <div className="col-6">
          <Skills />
          <Achievements />
          <Languages />
        </div>
      </div>
      <div className="row">
        <div className="col-1 start-devision-blue">
          <StartDevision />
        </div>
        <div className="col-5">
          <Education />
        </div>
        <div className="col-6"></div>
      </div>
      <div className="row">
        <div className="col-1 offset-11">
          <OverlayTrigger trigger="focus" placement="top" overlay={popover1}>
            <Link
              className="link-to-download"
              to={isLinkedSeen && isLaikaSeen ? "/lastPage" : undefined}
            >
              <Button
                className="home-page-button"
                title="DOWNLOAD"
                click={
                  isLinkedSeen && isLaikaSeen ? handleDownloadPdf : undefined
                }
              />
            </Link>
          </OverlayTrigger>
        </div>
      </div>
    </div>
  );
};

export default EditWebDevelopmentCV;
