import { v4 as uuid } from "uuid";

const tips = [
  {
    LinkedIn: {
      LinkedInAbout:
        "Make sure that your skills. EG., If you are an entry level PHP developer, don`t say that you are a PHP expert. Skip the abbrevation ant titles - keep it short and modest. Be concise and easy to understand. Don`t use overdone motivational quotes about work ethics and inspiration, try to make the `about` section as personal and as reflective of you as possible. Use the new `Open for business` feature. State your area of expertise and industry, to let reqruters know thay can reach you. Don`t write in things that you wouldn`t like to happen. For example, if you say you work good under pressure, employers might have an ace up in there sleeve when stucking a lot of work with a short deadline, becose you said so. Value your skills and free time accordingly",
      LinkedInArticles:
        "Tip: Add connections from the same field of work as you, even people you don`t know personaly. That way, recruters have a bigger chance of comming accros your profile. Put in the skills you are most experianced in, so you can be endorsed from other people. Tip: Ask friends and coworkers to endorse you.",
      LinkedInExperiance:
        "Explain every work experiance you`ve had in detail - what were your obligations and tasks, what was your job title, whitch technologies and tools did you use etc. Don`t be shy on listing experiance that that isn`t connected to your current profession. Volunteering at a local community center, summer jobs, projects you were a part of, pro bono work you did to improve your skills and gain experiance - everithing counts. Plus, every experiance serves as proof to your work ethics and adaptability. Tip: if you have no experiance and education to build up your profile, do some pro bono work or volonteer. You can help someone, and at the same time enrich your portfolio",
      LinkedInEducation:
        "Under education, list all the formal and non-formal education you have, with focus on the education that is relevant to your current title. For exemple, if you put Brainster Coding Academy as an education, list all the modules and project you have worked on. Make sure to list you Major and Bachelor/Master/Doctor thesis subject under the university education",
      LinkedInInterests:
        "List all your relevant accomplishments. Don`t say you have a black belt in karate, but mention an award from hakaton.",
    },
    Laika: {
      LaikaEmail: "Tip: Make sure you leave an email that you check regularly",
      LaikaSocialMedia:
        "Link all your social media and portfolios you want companies and recruiters to be able to see (LinkedIn, Facebook, etc.)",
      LaikaIndustry:
        "*You can only pick 1 industry out of the given 9 (Software Engineering, Design, Marketing and Communication, Data Science, IT and Sysadmin, Sales and Business Development, HR and Recruitment, Project and Product Management, Customer Support).",
      LaikaExpertise:
        "*You can pick up to 5 options in the Expertise field. Make sure your choices are realistic and they truly reflect the skills you are most confident in.",
      LaikaSalary:
        "*Select your desired salary. Don’t try to be too accessible, but please be realistic – make sure you desired salary is in accordance to your experience.",
      LaikaJobPlan:
        "*You can choose multiple choices for your desired job plan",
      LaikaLocations: "*You can choose out of four locations, or multiple",
      LaikaJobType: "*You can choose between a job in office, remote, or both.",
      LaikaJobTitle: "*Let the companies know what is your current job title.",
      LaikaOpportunity:
        "*Be honest about the motivation behind looking for a new opportunity. This will help us and the companies searching for new employees.",
      LaikaExperienceLevel:
        "*Please be honest when selecting the level of your experience",
      LaikaEducation:
        "Education is also not mandatory, but it adds weight to your portfolio if you have any academic knowledge.",
      LaikaOpertunity:
        "Be realistic when choosing the reason why you have a portfolio on Laika. This will help recruiters know whether you are suitable for the position on the long run.",
      LaikaTechnologies:
        "*Choose up to 8 technologies, but make sure you really know their ins and outs. The matching algorithm connects you to companies whose job openings have precise technologies listed.",
      LaikaWorkExperiance:
        "Work experience is not mandatory, but it helps companies know what kind of experience and in which industry you have.",
    },
    webDevelopment: {
      aboutMe:
        "Write a short intro that truly represents you –not cheesy quotes, but something you truly believe in. Don’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.",
      photo:
        "Your photo should be professional. It’s better to send a CV without a photo, that with one that makes you seem unserious.",
      socialNetworks:
        "Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. But don’t leave your home address. Leave a link to your LinkedIn profile. If you have proof of your previous job experiences online, link that too.",
      positionCompany:
        "Write your previous job experience and if you’re currently working, your current job titles. Explain your responsibilities and projects and list the tech stack and products/projects you worked on. If you were working in a team, let the recruiter know what your role was.",
      workingExpDates:
        "Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last.",
      accomplishmentsTasks:
        "Write your previous job experience and if you’re currently working, your current job titles. Explain your responsibilities and projects and list the tech stack and products/projects you worked on. If you were working in a team, let the recruiter know what your role was.",
      skills:
        "Showcase your tech stack . List the stack under every previous project you have worked on, or showcase your projects on GitHub. The easiest way to do this is to list the stack under a previous project that you have worked on. Another way is to showcase your projects on GitHub.",
      education:
        "Education is an important section – but be sure to select only what is relevant to the job you’re applying for. Write about your university degrees, relevant courses and if you have any publications, but skip the high and elementary school.",
      achievements:
        "Immediately should be your Projects/Publications section. In the tech industry, the focus should be on what you have created. Include data analysis projects, machine learning projects, and if possible, published scientific articles or tutorials.      Pick projects with relevance and connection to the job you’re applying for. They should demonstrate your technical skills and how they are applicable to solving real problems.",
      languages:
        "All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.",
    },
    dataScience: {
      photo:
        "Put a photo made with a good camera. It’s better to send a CV without a photo, if you were planning on cropping yourself from a group photo from a dinner with friends, or a selfie in you room.",
      aboutMe:
        "Write a short intro that truly represents you –not cheesy quotes, but something you truly believe in. Don’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.",
      socialNetworks:
        "Include your LinkedIn profile link, but don’t just copy and paste the whole profile URL, shorten it. Add a GitHub link or personal profile link to your contact information, and make it clickable. You’re applying for data science jobs, so most employers are going to look at your portfolio to see what kinds of projects you’re working on. Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. Don’t leave your home address.",
      positionCompany:
        "Write your previous job experience, and if you’re currently working, your current job title. Explain your responsibilities and projects and list the tech stack and products/projects you worked on. If you were working in a team, let the recruiter know what your role was.",
      workingExpDates:
        "Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last. We want to enable the reader to see what he needs to about your experience right away. Describe every job title with a few words about your most relevant experiences connected to the title you’re applying for.",
      education:
        "Let the recruiter know about your education , but only write the important parts – skip the high school and elementary, stick to the university degrees and courses relevant to the job you’re applying for.",
      dataScienceProjects:
        "Immediately should be your Projects/Publications section. In the tech industry, the focus should be on what you have created. Include data analysis projects, machine learning projects, and if possible, published scientific articles or tutorials.      Pick projects with relevance and connection to the job you’re applying for. They should demonstrate your technical skills and how they are applicable to solving real problems.",
      languages:
        "All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.",
      achievements:
        "Immediately should be your Projects/Publications section. In the tech industry, the focus should be on what you have created. Include data analysis projects, machine learning projects, and if possible, published scientific articles or tutorials.      Pick projects with relevance and connection to the job you’re applying for. They should demonstrate your technical skills and how they are applicable to solving real problems.",
      informalEducation:
        "Don’t be afraid to include a “Informal Education” section, where you can list all the courses, podcasts and webinars that you used to teach yourself about software development. Don’t include everything – just the education that is relevant and necessary for the position you are applying for.",
    },
    digitalMarketing: {
      photo:
        "Put a photo that looks professional – made with a good camera and showing you in a warm light. It’s better to send a CV without a photo, than a photo that makes you seem unprofessional.",
      aboutMe:
        "Know your unique value proposition and communicate it effectively. You need a personal tagline that will help you stand out from everyone else. This line should be the first impression the hiring manager will get from you. E.g. if you’re a Content Writer, don’t just say ‘’I’m a great content writer’’. Be creative and say why they should hire you to handle their content.",
      socialNetworks:
        "Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. Don’t leave your home address.",
      positionCompany:
        "List the job experiences that are relevant to the position you’re applying for. If you’re currently working, your current job titles. Explain your responsibilities and projects and list the strategies and projects you created or worked on.Tailor your CV to the company and position you’re applying for. Highlight the keywords from the job specification on your CV. For example, if you’re applying for an e-commerce role, include keywords such as ‘’Conversion, bounce rate and Google Analytics’’.Talk about campaigns you worked on, the budget you had, how it benefited the company etc. This will help recruiters learn how you could benefit them based on your past experience.",
      workingExpDates:
        "Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last. Enable the reader to see what he needs to about your experience right away. Describe every job title with a few words about your most relevant experiences connected to the title you’re applying for",
      education:
        "Under education, list only the relevant studies and courses , like university degree or Digital Marketing Academy. Don’t write where you went to high school.",
      skills:
        "Analyze what your key skills are. Make sure to list the skills you can shine in from day one. Showcase all the tools and technologies you know how to use . Everything that’s useful to a marketer: from Typeform and Canva, to Photoshop and Data Studio. Link campaigns you worked on If possible.",
      languages:
        "All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.",
      informalEducation:
        "Don’t be afraid to include a “Self-Study” where you can list all the courses, conferences, workshops and webinars that you used to teach yourself about digital marketing. Don’t include everything – just the education that is relevant and necessary for the position you are applying for.",
    },
    design: {
      aboutMe:
        "Write a short intro that truly represents you –not cheesy quotes, but something you truly believe in. Don’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.",
      positionCompany:
        "Describe every job experience with a few words about your most relevant responsibilities and projects connected to the title you’re applying for.",
      workingExpDates:
        "Dates are a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). First write your most recent experience, and the oldest ones last. We want to enable the reader to see what he needs to about your experience right away.",
      socialNetworks:
        "Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. Don’t leave your home address. Leave a link to your LinkedIn profile, or Behance/Dribbble/WiX. Make sure it’s clickable, and shorten it.",
      education:
        "List relevant education , including workshops or lectures you have visited. If you have a bachelor in Sports, and now you are applying as a designer because you finished a course, the course should be the highest in your “education” section. Don’t put in high and elementary school.",
      skills: "List all the skills, tools and technologies that you know well.",
    },

    editWebDevelopment: {
      basicInfo: {
        name: "",
        profession: "",
        aboutMe: "",
        email: "",
        phoneNumber: "",
        city: "",
        twitter: "",
        linkedIn: "",
      },

      workExperiance: [
        {
          id: uuid(),
          title: "",
          company: "",
          accomplishments: "",
          accomplishment1: "",
          task1: "",
          task2: "",
          startDate: "",
          endDate: "",
        },
      ],

      education: [
        {
          id: uuid(),
          studyProgram: "",
          institution: "",
          courses: "",
          thingLearned1: "",
          thingLearned2: "",
          thingLearned3: "",
        },
      ],

      skills: [
        {
          id: uuid(),
          skill: "",
        },
      ],

      achievements: [
        {
          id: uuid(),
          achievement: "",
          achievementDesc: "",
        },
      ],

      languages: [
        {
          id: uuid(),
          language: "",
          level: "",
        },
      ],
      dataScienceSkills: [
        {
          id: uuid(),
          dataScienceSkill: "PHP",
          level: "3",
        },
        {
          id: uuid(),
          dataScienceSkill: "Java",
          level: "1",
        },
        {
          id: uuid(),
          dataScienceSkill: "C++",
          level: "5",
        },
      ],
      informalEducations: [
        {
          id: uuid(),
          informalEducation: "",
        },
      ],
    },
  },
];

export default tips;
